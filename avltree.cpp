#include <iostream>
#include <malloc.h>

template <class T>
class Node{
public:
    T val;
    fpos_t pos;
    Node *r,*l;
    //int fathercount;
    Node(T v){
        val = v;
        r = nullptr;
        l = nullptr;
        //fathercount = 0;
    }
    Node(Node &node){
        r = node.r;
        l = node.l;
        pos = node.pos;
        val = node.val;
        //fathercount = node.fathercount;
    }
};


template <class T>
class AVL{
public:
    Node<T> *root = nullptr;
    AVL(){}
    void insert(Node<T> obj){
        if(root == nullptr){
            root = &obj;
            return;
        }
        Node<T> *curr = root;
        Node<T> *past = nullptr;
        while(curr != nullptr){
            past = curr;
            if(curr->val < obj.val){
                curr = curr->r;
            }else if(curr->val >= obj.val) {
                curr = curr->l;
            }
        }
        if(past->val < obj.val){
            past->r = &obj;
        }else {
            past->l = &obj;
        }
        //obj.fathercount = (past -> fathercount) + 1;
    }
    void inorder(Node<T> *p){
        if(p == nullptr){
            return;
        }
        inorder(p->l);
        std::cout<< p->val <<" ";
        inorder(p->r);
        return;
    }
    void LL(){
        Node<T> *p = root;
        Node<T> *lf = root->l;
        Node<T> *leg = lf->r;
        root = lf;
        root->r = p;
        p->l = leg;
    }
    void RR(){
        Node<T> *p = root;
        Node<T> *rf = root->r;
        Node<T> *leg = rf->l;
        root = rf;
        root->l = p;
        p->r = leg;
    }
    void LR(){
        Node<T> *p = root;
        Node<T> *lf = root->l;
        Node<T> *lr = lf->r;
        Node<T> *leg = lr->l;
        root->l = lr;
        lr->l = lf;
        lf->r = leg;
        LL();
    }
    void RL(){
        Node<T> *p = root;
        Node<T> *rf = root->r;
        Node<T> *rl = rf->l;
        Node<T> *leg = rl->r;
        root->r = rl;
        rl->r = rf;
        rf->l = leg;
        RR();
    }


};
int main(){
    AVL<int> avl;
    avl.insert(*(new Node<int> (0)));
    avl.insert(*(new Node<int> (1)));
    avl.insert(*(new Node<int> (2)));
    avl.insert(*(new Node<int> (3)));
    avl.insert(*(new Node<int> (4)));
    avl.inorder(avl.root);

    return 0;
}
