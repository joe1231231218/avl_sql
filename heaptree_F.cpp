#include <iostream>
#include <malloc.h>

typedef int LL;

class NodeH{
public:
    LL val;
    NodeH *r,*l,*fa;
    NodeH(LL v){
        val = v;
        r = nullptr;
        l = nullptr;
        fa = nullptr;
    }
    NodeH(NodeH &node,NodeH *father){
        r = node.r;
        l = node.l;
        fa = father;
        val = node.val;
    }
};

class heap{
public:
    NodeH *root = nullptr;
    NodeH *curr = nullptr;
    NodeH *far_left = nullptr;
    heap(){}

    void swap_father(NodeH *cu){
        LL tmp = cu->val;
        cu->val = cu->fa->val;
        cu->fa->val = tmp;
    }

    int judge(NodeH *cu){
        if(cu == (cu->fa->l)){
            return 0;
        }else if(cu == (cu->fa->r)){
            return 1;
        }else{
            std::cout<<"orphan at"<<cu<<" her father is "<<cu->fa<<std::endl;
            return 2;
        }
    }

    void insert(NodeH obj){
        if(root == nullptr){
            root = (new NodeH(obj, nullptr));
            curr = root;
            far_left = curr;
            return;
        }else if(root == curr){
            root -> l = (new NodeH(obj,root));
            curr =  root -> l;
            far_left = root -> l;
            if((curr->val) > (curr->fa->val)){
                swap_father(curr);
            }
            return;
        }
        NodeH *cu = curr;
        while(true) {
            if (judge(cu) == 0) {
                cu = cu->fa;
                break;
            } else {
                cu = cu->fa;
                if(cu == root){
                    cu = far_left;
                    break;
                }
            }
        }
        if(far_left == cu){
            far_left->l = new NodeH(obj,far_left);
            far_left = far_left->l;
            cu = far_left;
        }else{
            if(cu->r == nullptr){
                cu->r = new NodeH(obj,cu);
                cu = cu->r;
            }else{
                cu = cu->r;
                while(cu->l != nullptr){
                    cu = cu->l;
                }
                cu->l = new NodeH(obj,cu);
                cu = cu->l;
            }
        }
        curr = cu;
        while(cu != root){
            if((cu->val) > (cu->fa->val)){
                swap_father(cu);
            }else{
                break;
            }
            cu = cu->fa;
        }
        return;
    }
    void inorder(NodeH *p){
        if(p == nullptr) {
            return;
        }
        inorder(p->l);
        std::cout<< p->val <<" ";
        inorder(p->r);
        return;
    }

    void dele(){
        NodeH *cu = curr;
        root->val = cu->val;
        if (judge(cu) == 1) {
            cu = cu->fa->l;
        } else {
            while(true) {
                cu = cu->fa;
                if(cu == root){
                    while(cu->r != nullptr){
                        cu = cu->r;
                    }
                    if(cu->l == nullptr){
                        break;
                    } else {
                        cu = cu->l;
                    }
                    break;
                }
            }
        }

        if(judge(curr)==0){
            curr->fa->l = nullptr;
        } else {
            curr->fa->r = nullptr;
        }
        delete(curr);
        curr = cu;
        cu = root;
        while(true){
            if(cu->r == nullptr || cu->l == nullptr){
                break;
            }
            if(cu->val < cu->l->val || cu->val < cu->r->val ){
                if(cu->r->val < cu->l->val){
                    swap_father(cu->l);
                    cu = cu->l;
                } else {
                    swap_father(cu->r);
                    cu = cu->r;
                }
            } else {
                break;
            }
        }
    }
};


int main(){
    heap data;
    data.insert(*(new NodeH (0)));
    data.insert(*(new NodeH (1)));
    data.insert(*(new NodeH (2)));
    data.insert(*(new NodeH (3)));
    data.insert(*(new NodeH (4)));
    data.insert(*(new NodeH (5)));
    data.insert(*(new NodeH (6)));
    data.inorder(data.root);
    data.dele();
    data.inorder(data.root);
    heap he;
    for(int i;i<65535;++i){
        he.insert(*(new NodeH (i)));
    }


    return 0;
}
